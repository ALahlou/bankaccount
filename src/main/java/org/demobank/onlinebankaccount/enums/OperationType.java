package org.demobank.onlinebankaccount.enums;

public enum OperationType {

	DEPOSIT,
	WITHDRAWAL
}
