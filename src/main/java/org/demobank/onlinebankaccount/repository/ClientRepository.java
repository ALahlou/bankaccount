package org.demobank.onlinebankaccount.repository;

import org.demobank.onlinebankaccount.entity.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<ClientEntity, Long>{

}
