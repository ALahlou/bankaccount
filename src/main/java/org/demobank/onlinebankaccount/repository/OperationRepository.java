package org.demobank.onlinebankaccount.repository;

import java.util.List;

import org.demobank.onlinebankaccount.entity.OperationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationRepository extends JpaRepository<OperationEntity, Long>{

	List<OperationEntity> findByClientId(Long id);

}
