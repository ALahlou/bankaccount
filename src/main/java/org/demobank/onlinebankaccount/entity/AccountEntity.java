package org.demobank.onlinebankaccount.entity;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Transactional
@Table(name = "account")
public class AccountEntity {
	
	@Id
	@GeneratedValue
	private Long id;
	private Date creationDate;
	private Double accountBalance;
	
	@ManyToOne
	private ClientEntity client;

}
