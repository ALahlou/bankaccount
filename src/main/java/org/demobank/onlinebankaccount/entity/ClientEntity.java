package org.demobank.onlinebankaccount.entity;


import java.util.List;

import org.springframework.data.annotation.Id;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Transactional
@Table(name = "client")
public class ClientEntity {

	

	@Id
	@GeneratedValue
	private Long id;
	
	private String firstName;
	private String  lastName;
	
	@OneToMany(mappedBy = "client")
	private List<AccountEntity> accounts;
	
	@OneToMany(mappedBy = "client")
	private List<AccountEntity> operations;
	
	
}
