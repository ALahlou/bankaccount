package org.demobank.onlinebankaccount.entity;

import java.util.Date;

import org.demobank.onlinebankaccount.enums.OperationType;
import org.springframework.data.annotation.Id;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Transactional
@Table(name = "operation")
public class OperationEntity {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private ClientEntity client;
	
	@ManyToOne
	private AccountEntity account;
	
	private Date operationDate;
	
	private OperationType operationType;
	
	private Double amount;
	
}
