package org.demobank.onlinebankaccount.service;

import java.util.List;
import java.util.Optional;

import org.demobank.onlinebankaccount.entity.ClientEntity;
import org.demobank.onlinebankaccount.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepo;
	
	
    public ClientService(ClientRepository clientRepo) {
        this.clientRepo = clientRepo;
	}
	
	public ClientEntity createClient(ClientEntity client) {
		return clientRepo.save(client);
	}
	
	public List<ClientEntity> getClientsList() {
		return clientRepo.findAll();
	}
	
	public ClientEntity getClientById(Long id) {
		return clientRepo.findById(id).get();
	}
	
	@SuppressWarnings("null")
	public ClientEntity updateClient(ClientEntity updatedClient) {
		Optional<ClientEntity> optionalClient = clientRepo.findById(updatedClient.getId());
		if(optionalClient.isPresent()) {
			ClientEntity existingClient = null;
			existingClient.setFirstName(updatedClient.getFirstName());
			existingClient.setLastName(updatedClient.getLastName());
			existingClient.setAccounts(updatedClient.getAccounts());
			return clientRepo.save(existingClient);
		}
		return null;
	}
}
