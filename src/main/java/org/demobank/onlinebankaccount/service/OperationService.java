package org.demobank.onlinebankaccount.service;

import java.util.List;

import org.demobank.onlinebankaccount.entity.AccountEntity;
import org.demobank.onlinebankaccount.entity.OperationEntity;
import org.demobank.onlinebankaccount.repository.AccountRepository;
import org.demobank.onlinebankaccount.repository.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationService {

	@Autowired
	OperationRepository operationRepo;
	AccountService accountService;
	AccountRepository accountRepository;

	public OperationService(OperationRepository operationRepo, AccountService accountService,
			AccountRepository accountRepository) {
		this.operationRepo = operationRepo;
		this.accountService = accountService;
		this.accountRepository = accountRepository;
	}

	public OperationEntity createOperation(OperationEntity operation) {
		AccountEntity account = accountRepository.findById(operation.getClient().getId()).get();
		Double operationAmount = operation.getAmount();
		Double accountBalance = account.getAccountBalance();
		
		if (operation.getOperationType().equals("DEPOSIT")) {
			accountBalance = accountBalance + operationAmount;
		} else if (operation.getOperationType().equals("WITHDRAWAL") && accountBalance > operationAmount) {
			accountBalance = accountBalance - operationAmount;
		}
		
		account.setAccountBalance(accountBalance);
		accountService.updateAccount(account);
		return operationRepo.save(operation);
	}

	public List<OperationEntity> getAllOperations() {
		return operationRepo.findAll();
	}

	public List<OperationEntity> getOperationByClientId(Long id) {

		return operationRepo.findByClientId(id);
	}

}
