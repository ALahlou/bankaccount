package org.demobank.onlinebankaccount.service;

import java.util.Optional;

import org.demobank.onlinebankaccount.entity.AccountEntity;
import org.demobank.onlinebankaccount.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

	@Autowired
	AccountRepository accountRepo;
	
	
	public AccountService(AccountRepository accountRepo) {
		 this.accountRepo = accountRepo;
	}
	
	public AccountEntity createAccount(AccountEntity account) {
		return accountRepo.save(account);
	}
	
	public AccountEntity getAccountById(Long id) {
		return accountRepo.findById(id).get();
	}
	
	public AccountEntity updateAccount(AccountEntity account) {
		Optional<AccountEntity> optionalAccount = accountRepo.findById(account.getId());
		if(optionalAccount.isPresent()) {
			return accountRepo.save(account);
		}else 
			throw new IllegalArgumentException();
	}
	
	public ResponseEntity<HttpStatus> deleteAccount(AccountEntity account) {
		try {
			accountRepo.deleteById(account.getId());
		return new ResponseEntity<>(HttpStatus.ACCEPTED);	
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
	}
}
