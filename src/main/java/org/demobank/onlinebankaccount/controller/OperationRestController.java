package org.demobank.onlinebankaccount.controller;

import java.util.List;

import org.demobank.onlinebankaccount.entity.OperationEntity;
import org.demobank.onlinebankaccount.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/operation")
public class OperationRestController {

	@Autowired
	OperationService operationService;
	
	
	
	
	@PostMapping("/create")
	public OperationEntity createOperation(@RequestBody OperationEntity operation) {
		return operationService.createOperation(operation);
	}
	
	@GetMapping("/getOperations")
	public List<OperationEntity>getAllOperations(){
		return operationService.getAllOperations();
	}
	
	@GetMapping("/getOperationById")
	public List<OperationEntity> getOperationByClientId(@RequestBody Long id){
		return operationService.getOperationByClientId(id);
	}
}
