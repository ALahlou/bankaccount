package org.demobank.onlinebankaccount.controller;

import java.util.List;

import org.demobank.onlinebankaccount.entity.ClientEntity;
import org.demobank.onlinebankaccount.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/client")
public class ClientRestController {

	@Autowired
	private ClientService clientService;
	
	private ClientRestController(ClientService clientService) {
		this.clientService = clientService;
	}
	
	@PostMapping("/create")
	public ClientEntity createClient(@RequestBody ClientEntity client) {
		return clientService.createClient(client);
	}
	
	@GetMapping("/clientById")
	public ClientEntity getClientById(@RequestBody Long id) {
		return clientService.getClientById(id);
	}
	
	@GetMapping("/clients")
	public List<ClientEntity> getClientsList() {
		return clientService.getClientsList();
	}
	
	@PutMapping
	public ClientEntity updateClient(@RequestBody ClientEntity client) {
		return clientService.updateClient(client);
	}
}
