package org.demobank.onlinebankaccount.controller;

import org.demobank.onlinebankaccount.entity.AccountEntity;
import org.demobank.onlinebankaccount.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account")
public class AccountRestController {

	@Autowired
	private AccountService accountService;
	
	
	
	private AccountRestController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	@PostMapping("/create")
	public AccountEntity createAccount(@RequestBody AccountEntity account) {
		return accountService.createAccount(account);
	}
	
	@GetMapping("/getAccountById")
	public AccountEntity getAccountById(@RequestBody Long id) {
		return accountService.getAccountById(id);
	}
	
	@PutMapping("/updateAccount")
	public AccountEntity updateAccount(@RequestBody AccountEntity account) {
		return accountService.updateAccount(account);
	}
	
	@DeleteMapping("/deleteAccount")
	public ResponseEntity<HttpStatus> deleteAccount(@RequestBody AccountEntity account) {
		return accountService.deleteAccount(account);
	}
}
