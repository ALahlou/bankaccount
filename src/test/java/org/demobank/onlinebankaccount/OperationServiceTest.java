package org.demobank.onlinebankaccount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.demobank.onlinebankaccount.entity.AccountEntity;
import org.demobank.onlinebankaccount.entity.OperationEntity;
import org.demobank.onlinebankaccount.enums.OperationType;
import org.demobank.onlinebankaccount.repository.AccountRepository;
import org.demobank.onlinebankaccount.repository.OperationRepository;
import org.demobank.onlinebankaccount.service.AccountService;
import org.demobank.onlinebankaccount.service.OperationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class OperationServiceTest {

    private OperationService operationService;

    @Mock
    private OperationRepository operationRepository;

    @Mock
    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        operationService = new OperationService(operationRepository, accountService, accountRepository);
    }

    @Test
    public void testCreateOperation_deposit() {
        // Préparation des données de test
        OperationEntity operation = new OperationEntity();
        operation.setOperationType(OperationType.DEPOSIT);
        operation.setAmount(100.0);
        
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        account.setAccountBalance(200.0);
        
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account));
        
        // Exécution de la méthode à tester
        OperationEntity createdOperation = operationService.createOperation(operation);
        
        // Vérification des résultats
        assertEquals(300.0, account.getAccountBalance());
        verify(accountService, times(1)).updateAccount(account);
        verify(operationRepository, times(1)).save(operation);
    }

    @Test
    public void testCreateOperation_withdrawal_sufficientBalance() {
        // Préparation des données de test
        OperationEntity operation = new OperationEntity();
        operation.setOperationType(OperationType.WITHDRAWAL);
        operation.setAmount(50.0);
        
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        account.setAccountBalance(100.0);
        
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account));
        
        // Exécution de la méthode à tester
        OperationEntity createdOperation = operationService.createOperation(operation);
        
        // Vérification des résultats
        assertEquals(50.0, account.getAccountBalance());
        verify(accountService, times(1)).updateAccount(account);
        verify(operationRepository, times(1)).save(operation);
    }

    @Test
    public void testCreateOperation_withdrawal_insufficientBalance() {
        // Préparation des données de test
        OperationEntity operation = new OperationEntity();
        operation.setOperationType(OperationType.WITHDRAWAL);
        operation.setAmount(200.0);
        
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        account.setAccountBalance(100.0);
        
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account));
        
        // Exécution de la méthode à tester
        OperationEntity createdOperation = operationService.createOperation(operation);
        
        // Vérification des résultats
        assertEquals(100.0, account.getAccountBalance());
        verify(accountService, times(0)).updateAccount(account);
        verify(operationRepository, times(0)).save(operation);
    }
    
    // Ajoutez d'autres tests unitaires selon vos besoins

}

