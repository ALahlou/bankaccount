package org.demobank.onlinebankaccount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.demobank.onlinebankaccount.entity.ClientEntity;
import org.demobank.onlinebankaccount.repository.ClientRepository;
import org.demobank.onlinebankaccount.service.ClientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ClientServiceTest {

    private ClientService clientService;

    @Mock
    private ClientRepository clientRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        clientService = new ClientService(clientRepository);
    }

    @Test
    public void testCreateClient() {
        // Préparation des données de test
        ClientEntity client = new ClientEntity();
        client.setId(1L);
        
        when(clientRepository.save(client)).thenReturn(client);
        
        // Exécution de la méthode à tester
        ClientEntity createdClient = clientService.createClient(client);
        
        // Vérification des résultats
        assertEquals(client, createdClient);
        verify(clientRepository, times(1)).save(client);
    }

    @Test
    public void testGetClientsList() {
        // Préparation des données de test
        List<ClientEntity> clients = new ArrayList<>();
        clients.add(new ClientEntity(1L, null, null, null, null));
        clients.add(new ClientEntity(2L, null, null, null, null));
        
        when(clientRepository.findAll()).thenReturn(clients);
        
        // Exécution de la méthode à tester
        List<ClientEntity> result = clientService.getClientsList();
        
        // Vérification des résultats
        assertEquals(clients, result);
        verify(clientRepository, times(1)).findAll();
    }

    @Test
    public void testGetClientById_existingClient() {
        // Préparation des données de test
        Long clientId = 1L;
        ClientEntity client = new ClientEntity(clientId, null, null, null, null);
        
        when(clientRepository.findById(clientId)).thenReturn(Optional.of(client));
        
        // Exécution de la méthode à tester
        ClientEntity result = clientService.getClientById(clientId);
        
        // Vérification des résultats
        assertEquals(client, result);
        verify(clientRepository, times(1)).findById(clientId);
    }

    @Test
    public void testGetClientById_nonExistingClient() {
        // Préparation des données de test
        Long clientId = 1L;
        
        when(clientRepository.findById(clientId)).thenReturn(Optional.empty());
        
        // Exécution de la méthode à tester
        assertThrows(NoSuchElementException.class, () -> clientService.getClientById(clientId));
        verify(clientRepository, times(1)).findById(clientId);
    }

    @Test
    public void testUpdateClient_existingClient() {
        // Préparation des données de test
        Long clientId = 1L;
        ClientEntity existingClient = new ClientEntity(clientId, null, null, null, null);
        ClientEntity updatedClient = new ClientEntity(clientId, null, null, null, null);
        
        when(clientRepository.findById(clientId)).thenReturn(Optional.of(existingClient));
        when(clientRepository.save(existingClient)).thenReturn(existingClient);
        
        // Exécution de la méthode à tester
        ClientEntity result = clientService.updateClient(updatedClient);
        
        // Vérification des résultats
        assertEquals(existingClient, result);
        verify(clientRepository, times(1)).findById(clientId);
        verify(clientRepository, times(1)).save(existingClient);
    }

    @Test
    public void testUpdateClient_nonExistingClient() {
        // Préparation des données de test
        Long clientId = 1L;
        ClientEntity updatedClient = new ClientEntity(clientId, null, null, null, null);
        
        when(clientRepository.findById(clientId)).thenReturn(Optional.empty());
        
        // Exécution de la méthode à tester
        ClientEntity result = clientService.updateClient(updatedClient);
        
        // Vérification des résultats
        assertNull(result);
        verify(clientRepository, times(1)).findById(clientId);
        verify(clientRepository, never()).save(any(ClientEntity.class));
    }
}
