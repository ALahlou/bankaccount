package org.demobank.onlinebankaccount;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.demobank.onlinebankaccount.entity.AccountEntity;
import org.demobank.onlinebankaccount.repository.AccountRepository;
import org.demobank.onlinebankaccount.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AccountServiceTest {

    private AccountService accountService;

    @Mock
    private AccountRepository accountRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        accountService = new AccountService(accountRepository);
    }

    @Test
    public void testCreateAccount() {
        // Préparation des données de test
        AccountEntity account = new AccountEntity();
        account.setId(1L);
        
        when(accountRepository.save(account)).thenReturn(account);
        
        // Exécution de la méthode à tester
        AccountEntity createdAccount = accountService.createAccount(account);
        
        // Vérification des résultats
        assertEquals(account, createdAccount);
        verify(accountRepository, times(1)).save(account);
    }

    @Test
    public void testGetAccountById() {
        // Préparation des données de test
        Long accountId = 1L;
        AccountEntity account = new AccountEntity(accountId, null, null, null);
        
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(account));
        
        // Exécution de la méthode à tester
        AccountEntity result = accountService.getAccountById(accountId);
        
        // Vérification des résultats
        assertEquals(account, result);
        verify(accountRepository, times(1)).findById(accountId);
    }

    @Test
    public void testUpdateAccount_existingAccount() {
        // Préparation des données de test
        Long accountId = 1L;
        AccountEntity existingAccount = new AccountEntity(accountId, null, null, null);
        AccountEntity updatedAccount = new AccountEntity(accountId, null, null, null);
        
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(existingAccount));
        when(accountRepository.save(existingAccount)).thenReturn(existingAccount);
        
        // Exécution de la méthode à tester
        AccountEntity result = accountService.updateAccount(updatedAccount);
        
        // Vérification des résultats
        assertEquals(existingAccount, result);
        verify(accountRepository, times(1)).findById(accountId);
        verify(accountRepository, times(1)).save(existingAccount);
    }

    @Test
    public void testUpdateAccount_nonExistingAccount() {
        // Préparation des données de test
        Long accountId = 1L;
        AccountEntity updatedAccount = new AccountEntity(accountId, null, null, null);
        
        when(accountRepository.findById(accountId)).thenReturn(Optional.empty());
        
        // Exécution de la méthode à tester
        assertThrows(IllegalArgumentException.class, () -> accountService.updateAccount(updatedAccount));
        verify(accountRepository, times(1)).findById(accountId);
        verify(accountRepository, never()).save(any());
    }

    @Test
    public void testDeleteAccount_success() {
        // Préparation des données de test
        Long accountId = 1L;
        AccountEntity account = new AccountEntity(accountId, null, null, null);
        
        // Exécution de la méthode à tester
        ResponseEntity<HttpStatus> response = accountService.deleteAccount(account);
        
        // Vérification des résultats
        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
        verify(accountRepository, times(1)).deleteById(accountId);
    }

    @Test
    public void testDeleteAccount_failure() {
        // Préparation des données de test
        Long accountId = 1L;
        AccountEntity account = new AccountEntity(accountId, null, null, null);
        
        doThrow(Exception.class).when(accountRepository).deleteById(accountId);
        
        // Exécution de la méthode à tester
        ResponseEntity<HttpStatus> response = accountService.deleteAccount(account);
        
        // Vérification des résultats
        assertEquals(HttpStatus.CONFLICT, response.getStatusCode());
        verify(accountRepository, times(1)).deleteById(accountId);
    }
}

        //
